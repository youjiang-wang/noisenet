# noiseNet

The implementation of the noiseNet architecture.

## notice
This is the code to build the noiseNet structure described in our paper "noiseNet: A neural network to predict marine propellers' underwater radiated noise", which has been submitted and waits for publishing.
