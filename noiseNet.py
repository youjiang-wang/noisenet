import sys
import io
import os.path

import tensorflow as tf
from tensorflow.keras.layers import Input, Dense, Conv1D, Flatten, Lambda
from tensorflow.keras.layers import Concatenate, RepeatVector, TimeDistributed
from tensorflow.keras.models import Model

class BladeFFTLayer(tf.keras.layers.Layer):
    '''
    A custom keras layer to perfrom fft transformation and choose of blade frequencies
    '''
    def __init__(self, n_per_period, BPF = 3):
        super(BladeFFTLayer, self).__init__()
        self.BPF = round(BPF)
        self.n_period = round(n_per_period)

        self.perm = [0, 2, 1]

    def call(self, inputs):
        x_seq = inputs[0]
        Z = inputs[1]
        x_1 = tf.signal.rfft(tf.transpose(x_seq, self.perm)[:, :, -self.n_period:])
        x_1 = tf.transpose(x_1, self.perm)

        ind = tf.cast(tf.round([[j*Z] for j in range(1, self.BPF+1)]), 'int32')
        ind = tf.transpose(ind, [2, 0, 1])
        x_1 = tf.gather_nd(params = x_1, indices = ind, batch_dims=1)
        x_1 = tf.math.abs(x_1)
        x_1 = tf.transpose(tf.math.multiply(tf.transpose(x_1), Z))

        x_1 = tf.math.divide(x_1, float(self.n_period)*0.5)

        return x_1
    
    def get_config(self):
        base_config = super(BladeFFTLayer, self).get_config()
        config = {'n_per_period': self.n_period, 'BPF': self.BPF}
        return dict(list(base_config.items()) + list(config.items()))

class noiseNet():
    @staticmethod
    def BuildModel(self, X_sequence_shape = (None, 118, 3, 6), 
                    X_scalar_shape = (None, 6), 
                    Y_shape = (None, 3),
                    L1Coef = 0.0, L2Coef = 5.0):
        '''
        build the ANN structure using tensorflow.
        
        Parameters:
        ===================================
        X_sequence_shape: shape of sequence input data, two revolutions of data are given
        X_scalar_shape: shape of scalar input data
        Y_shape: shape of output data

        Returns:
        ===================================
        A tensorflow ANN model with the noiseNet architecture
        '''
        T_x, r_x, n_seq = X_sequence_shape[-3], X_sequence_shape[-2], X_sequence_shape[-1]
        n_scalar = X_scalar_shape[-1]
        n_y = Y_shape[-1]
        Z_id = 1 # position of Z in scalar input
        Z_offset = 4 # used to restore the real value of Z
    
        Sequence_input = Input((T_x, r_x, n_seq), name='sequence_input')
        Scalar_input = Input((n_scalar,), name = 'scalar_input')
    
        regularizer = tf.keras.regularizers.L1L2(L1Coef, L2Coef)
        # block-1, separate radius
        X = TimeDistributed(TimeDistributed(Dense(16, 'tanh', kernel_regularizer=regularizer)))(Sequence_input)
        X = TimeDistributed(TimeDistributed(Dense(16, 'tanh', kernel_regularizer=regularizer)))(X)
        X = TimeDistributed(TimeDistributed(Dense(16, 'tanh', kernel_regularizer=regularizer)))(X)
        X = TimeDistributed(Flatten())(X)

        # block-2, all radii
        Other = RepeatVector(T_x)(Scalar_input)
        X = Concatenate(axis=-1)([X, Other])
        X = TimeDistributed(Dense(32, 'tanh', kernel_regularizer=regularizer))(X)
        X = TimeDistributed(Dense(16, 'tanh', kernel_regularizer=regularizer))(X)
        X = TimeDistributed(Dense(4, 'tanh', kernel_regularizer=regularizer))(X)
    
        # CNN + FFT + Log
        # because 2 revolutions of data is supplied, T_x/2 is the number of data in one propeller roation period
        X = Conv1D(2, 5, kernel_regularizer=regularizer)(X)
        X = BladeFFTLayer(n_per_period=T_x/2)([X, Scalar_input[:, Z_id] + Z_offset])
        X = Lambda(lambda x: tf.math.log(x))(X)
    
        # block-3
        Other = RepeatVector(3)(Scalar_input)
        X = Concatenate(axis=-1)([X, Other])
        X = TimeDistributed(Dense(units = 16, activation = 'tanh', kernel_regularizer=regularizer))(X)
        X = TimeDistributed(Dense(units = 16, activation = 'tanh', kernel_regularizer=regularizer))(X)
        X = TimeDistributed(Dense(units = 16, activation = 'tanh', kernel_regularizer=regularizer))(X)
        X = Flatten()(X)
        
        # block-4
        X = Dense(units = 8, activation = 'tanh', kernel_regularizer=regularizer)(X)
   
        # ouput block
        X = Dense(units = n_y, kernel_regularizer=regularizer)(X)
        X = Lambda(lambda x: 30*x + 170)(X)
    
        model = Model(inputs = [AP_input, Other_input], outputs = X)
        return model
    
if __name__ == "__main__":
    model = BuildModel()
    model.summary()
    
